<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MobileUserFile */
?>
<div class="mobile-user-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mobile_user_id',
            'name',
            'path',
            'type',
            'created_at',
        ],
    ]) ?>

</div>
