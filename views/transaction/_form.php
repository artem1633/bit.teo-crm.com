<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mobile_user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\MobileUser::find()->all(), 'id', 'login')) ?>

    <?= $form->field($model, 'wallet_from_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\MUserWallet::find()->all(), 'id', 'token')) ?>

    <?= $form->field($model, 'wallet_to')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(\app\models\Transaction::getTypes()) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'commission')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\Transaction::getStatuses()) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
