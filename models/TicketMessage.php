<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\User;
use app\components\FcmPusher;

/**
 * This is the model class for table "ticket_message".
 *
 * @property int $id
 * @property int $ticket_id Тикет
 * @property string $text Текст
 * @property int $from Кто отправил
 * @property string $created_at Дата и время
 *
 * @property Ticket $ticket
 */
class TicketMessage extends \yii\db\ActiveRecord
{
    const FROM_MANAGER = 0;
    const FROM_USER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_message';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'from'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticket_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Тикет',
            'text' => 'Текст',
            'from' => 'Кто отправил',
            'created_at' => 'Дата и время',
        ];
    }

    public function afterSave($insert, $attributes)
    {
        parent::afterSave($insert, $attributes);

        if($this->from == self::FROM_MANAGER){
            $ticket = $this->ticket;
            $recipient = MobileUser::find()->where(['id' => $ticket->mobile_user_id])->one();

            $url = 'https://fcm.googleapis.com/fcm/send';
            $recipients = $recipient->fcm_token;
            $message = $this->text;

            $request_body = [
                'to' => $recipients,
                'notification' => [
                    'ticket_id' => $ticket->id,
                    'title' => 'Новое сообщение в тиките',
                    'body' => $message,
                    'data' => json_encode(['ticket_id' => $ticket->id, 'body' => $message]),
                ],
                'data' => ['ticket_id' => $ticket->id, 'body' => $message],
            ];
            $fields = json_encode($request_body);

            $request_headers = [
                'Content-Type: application/json',
                'Authorization: key=AIzaSyBeCsskPbMBTgpTBD3z8LzjsVf7H86nLPk',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($ch);

            Yii::warning($response);

            curl_close($ch);
        } else {
            $users = User::find()->where(['role' => 1])->andWhere(['!=', 'fcm_token', null])->all();
            foreach($users as $user)
            {
                if($user){
                    if($user->fcm_token){
                        FcmPusher::push($user->fcm_token, 'Новое сообщение в тиките', [], false);   
                    }
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id']);
    }
}
