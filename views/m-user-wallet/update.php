<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MUserWallet */
?>
<div class="muser-wallet-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
