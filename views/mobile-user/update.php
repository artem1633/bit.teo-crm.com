<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MobileUser */
?>
<div class="mobile-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
