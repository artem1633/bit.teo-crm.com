<?php

namespace app\modules\api\models;

use app\models\MobileUser;
use app\models\MUserWallet;
use Yii;
use yii\base\Model;

/**
 * Class RegisterModel
 * @package app\modules\api\models
 */
class RegisterModel extends Model
{
    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['login', 'password'], 'string'],
            [['email'], 'email'],
            ['email', 'unique', 'targetClass' => MobileUser::className(), 'targetAttribute' => 'email'],
        ];
    }

    /**
     * @return string
     */
    public function register()
    {
        if($this->validate()){
            $user = new MobileUser([
                'login' => $this->login,
                'password' => $this->password,
                'email' => $this->email,
            ]);
            $user->save(false);


            $password = Yii::$app->security->generateRandomString(10);
            $api_token = '4272f3cc-00da-4b61-832b-dae4d64fdef9';

            $ch = curl_init('localhost:3001/api/v2/create');

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "password={$password}&api_code={$api_token}&email={$user->email}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $info = json_decode(curl_exec($ch), true);

            curl_close($ch);

            $walletBTC = new MUserWallet([
                'mobile_user_id' => $user->id,
                'balance' => MUserWallet::START_BALANCE,
                'password' => $password,
                'guid' => $info['guid'],
                'address' => $info['address'],
                'type' => MUserWallet::TYPE_BTC
            ]);
            $walletBTC->save(false);

            $walletLTC = new MUserWallet([
                'mobile_user_id' => $user->id,
                'balance' => MUserWallet::START_BALANCE,
                'password' => $password,
                'guid' => $info['guid'],
                'address' => $info['address'],
                'type' => MUserWallet::TYPE_LTC
            ]);
            $walletLTC->save(false);

            $walletETH = new MUserWallet([
                'mobile_user_id' => $user->id,
                'balance' => MUserWallet::START_BALANCE,
                'password' => $password,
                'guid' => $info['guid'],
                'address' => $info['address'],
                'type' => MUserWallet::TYPE_ETH
            ]);
            $walletETH->save(false);

            return $user->token;
        }

        return null;
    }
}