<?php

use yii\db\Migration;

/**
 * Class m191017_155922_remove_rates_settings
 */
class m191017_155922_remove_rates_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\models\Settings::deleteAll(['key' => ['btc_rate', 'ltc_rate', 'eth_rate']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
