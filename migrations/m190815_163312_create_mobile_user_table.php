<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mobile_user`.
 */
class m190815_163312_create_mobile_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mobile_user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->comment('Логин'),
            'password' => $this->string()->comment('Пароль'),
            'email' => $this->string()->comment('Email'),
            'token' => $this->string()->comment('Токен'),
            'created_at' => $this->dateTime()->comment('Дата и время регистрации'),
            'last_login_datetime' => $this->dateTime()->comment('Дата и время последней авторизации'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mobile_user');
    }
}
