<?php

use yii\db\Migration;

/**
 * Handles adding tx_hash to table `transaction`.
 */
class m191024_120235_add_tx_hash_column_to_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('transaction', 'tx_hash', $this->string()->comment('Хэш транзакции'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('transaction', 'tx_hash');
    }
}
