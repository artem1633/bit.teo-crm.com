<?php

use yii\db\Migration;

/**
 * Handles adding push_token to table `mobile_user`.
 */
class m190908_180809_add_push_token_column_to_mobile_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('mobile_user', 'fcm_token', $this->string()->after('token')->comment('FCM токен'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('mobile_user', 'fcm_token');
    }
}
