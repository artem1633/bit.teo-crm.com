<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 */
class m190815_164300_create_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'mobile_user_id' => $this->integer()->comment('Пользователь'),
            'wallet_from_id' => $this->integer()->comment('Кошелек отправитель'),
            'wallet_to' => $this->string()->comment('Кошелек получателя'),
            'type' => $this->string()->comment('Тип операции'),
            'amount' => $this->float()->comment('Сумма'),
            'commission' => $this->float()->comment('Комиссия'),
            'status' => $this->string()->comment('Статус'),
            'comment' => $this->text()->comment('Комментарий'),
            'created_at' => $this->dateTime()->comment('Дата и время создания')
        ]);

        $this->createIndex(
            'idx-transaction-mobile_user_id',
            'transaction',
            'mobile_user_id'
        );

        $this->addForeignKey(
            'fk-transaction-mobile_user_id',
            'transaction',
            'mobile_user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-transaction-wallet_from_id',
            'transaction',
            'wallet_from_id'
        );

        $this->addForeignKey(
            'fk-transaction-wallet_from_id',
            'transaction',
            'wallet_from_id',
            'm_user_wallet',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-transaction-wallet_from_id',
            'transaction'
        );

        $this->dropForeignKey(
            'fk-transaction-mobile_user_id',
            'transaction'
        );

        $this->dropIndex(
            'idx-transaction-wallet_from_id',
            'transaction'
        );

        $this->dropIndex(
            'idx-transaction-mobile_user_id',
            'transaction'
        );

        $this->dropTable('transaction');
    }
}
