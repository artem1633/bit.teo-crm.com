<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m190815_170613_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'type' => $this->string()->defaultValue(\app\models\Settings::TYPE_TEXT)->comment('Тип'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);

        $this->insert('settings', [
            'key' => 'commission_buy_percent',
            'value' => '0',
            'label' => 'Комиссия за покупку (в проценте)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_buy_value',
            'value' => '0',
            'label' => 'Комиссия за покупку (в значении)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_transaction_percent',
            'value' => '0',
            'label' => 'Комиссия за перевод (в проценте)',
        ]);

        $this->insert('settings', [
            'key' => 'commission_transaction_value',
            'value' => '0',
            'label' => 'Комиссия за перевод (в значении)',
        ]);

        $this->insert('settings', [
            'key' => 'exchange_token',
            'value' => '',
            'label' => 'Токен биржи',
        ]);

        $this->insert('settings', [
            'key' => 'payment_system',
            'value' => '',
            'label' => 'Платежная система',
        ]);

        $this->insert('settings', [
            'key' => 'mobile_app_token',
            'value' => '',
            'label' => 'Токен для мобильного приложения',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
