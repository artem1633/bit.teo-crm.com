<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mobile_user_file".
 *
 * @property int $id
 * @property int $mobile_user_id Пользователь
 * @property string $name Наименование файла
 * @property string $path Путь
 * @property int $type Тип
 * @property string $created_at
 *
 * @property MobileUser $mobileUser
 */
class MobileUserFile extends \yii\db\ActiveRecord
{
    const TYPE_PASSPORT = 0;
    const TYPE_ID_CARD = 2;
    const TYPE_BY = 3;
    const TYPE_OTHER = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_user_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile_user_id', 'type'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'path'], 'string', 'max' => 255],
            [['mobile_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['mobile_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile_user_id' => 'Пользователь',
            'name' => 'Наименование файла',
            'path' => 'Путь',
            'type' => 'Тип',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PASSPORT => 'Паспорт',
            self::TYPE_ID_CARD => 'ID Карта',
            self::TYPE_BY => 'BY',
            self::TYPE_OTHER => 'Другой',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobileUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'mobile_user_id']);
    }
}
