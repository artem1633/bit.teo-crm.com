<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\MobileUserFile;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MobileUserFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

    <div class="popup-gallery">
        <?php foreach(MobileUserFile::getTypes() as $index => $name): ?>

            <div class="row">
                <div class="col-md-12">
                    <h5><?= $name ?></h5>
                    <?php foreach($dataProvider->models as $model): ?>
                        <?php if($model->type === $index): ?>
                            <a href="/<?=$model->path?>" title="<?=$name?>">
                                <img src="/<?=$model->path?>" style="height: 40px; width: 40px; object-fit: cover; margin-bottom: 3px;">
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>

        <?php endforeach; ?>
    </div>


<?php

$script = <<< JS
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Загрузка #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            tPrev: 'Предыдущая', // title for left button
            tNext: 'Следущая', // title for right button
            tCounter: '<span class="mfp-counter">%curr% из %total%</span>', // markup of counter
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">Изображение #%curr%</a> не найдено.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>