<?php

namespace app\modules\api\controllers;

use app\models\MobileUser;
use app\models\Ticket;
use app\models\TicketMessage;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use app\models\User;
use app\components\FcmPusher;

class TicketController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $tickets = Ticket::find()->where(['mobile_user_id' => $this->user->id])->all();

        return $tickets;
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new Ticket();

        if($model->load($data, 'model') && $model->validate())
        {
            $model->mobile_user_id = $this->user->id;
            $model->save(false);

            $users = User::find()->where(['role' => 1])->andWhere(['!=', 'fcm_token', null])->all();
            foreach($users as $user)
            {
                if($user){
                    if($user->fcm_token){
                        FcmPusher::push($user->fcm_token, 'Новый тикет', [], false);   
                    }
                }
            }
            return $model;
        } else {
            return $model->errors;
        }

        return ['result' => false];
    }

    /**
     * postId integer
     */
    public function actionGetMessages()
    {
        if(isset($_GET['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        $model = Ticket::findOne($_GET['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $messages = TicketMessage::find()->where(['ticket_id' => $model->id])->all();

        return $messages;
    }

    /**
     * postId integer
     * text string
     */
    public function actionSendMessage()
    {
        if(isset($_POST['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        if(isset($_POST['text']) == null){
            throw new BadRequestHttpException('text must be required');
        }

        /** @var Ticket $model */
        $model = Ticket::findOne($_POST['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        if($model->mobile_user_id != $this->user->id){
            throw new ForbiddenHttpException();
        }

        $message = new TicketMessage([
            'ticket_id' => $model->id,
            'from' => TicketMessage::FROM_USER,
            'text' => $_POST['text']
        ]);
        $result = $message->save(false);

        $model->updateLastMessageDateTime($message->created_at);

        if($result) {
            return $message;
        } else {
            return ['result' => false];
        }
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }
}