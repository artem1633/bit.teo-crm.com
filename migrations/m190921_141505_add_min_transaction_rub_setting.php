<?php

use yii\db\Migration;

/**
 * Class m190921_141505_add_min_transaction_rub_setting
 */
class m190921_141505_add_min_transaction_rub_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'min_transaction_buy_value_rub',
            'label' => 'Минимальное кол-во криптовалюты, которое можно купить (РУБ)',
            'value' => '100',
        ]);

        $this->insert('settings', [
            'key' => 'min_transaction_transfer_value_rub',
            'label' => 'Минимальное кол-во криптовалюты, которое можно перевести (РУБ)',
            'value' => '100',
        ]);

        $this->insert('settings', [
            'key' => 'min_transaction_commission_rub',
            'label' => 'Минимальный размер комиссии за покупку/перевод средств (РУБ)',
            'value' => '100',
        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => ['min_transaction_buy_value_rub', 'min_transaction_commission_rub', 'min_transaction_transfer_value_rub']]);
    }
}
