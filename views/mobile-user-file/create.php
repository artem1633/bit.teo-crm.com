<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MobileUserFile */

?>
<div class="mobile-user-file-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
