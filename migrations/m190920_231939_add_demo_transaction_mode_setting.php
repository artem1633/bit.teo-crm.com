<?php

use yii\db\Migration;

/**
 * Class m190920_231939_add_demo_transaction_mode_setting
 */
class m190920_231939_add_demo_transaction_mode_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'demo_transaction_mode',
            'label' => 'Демо режим при расчетах',
            'type' => \app\models\Settings::TYPE_CHECKBOX,
            'value' => 0,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'demo_transaction_mode']);
    }
}
