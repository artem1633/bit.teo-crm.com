<?php

use yii\db\Migration;

/**
 * Handles adding amount_rub to table `transaction`.
 */
class m190914_170804_add_amount_rub_column_to_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('transaction', 'amount_rub', $this->float()->after('amount')->comment('Сумма в рублях'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('transaction', 'amount_rub');
    }
}
