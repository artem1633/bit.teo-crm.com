<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MUserWallet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="muser-wallet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mobile_user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\MobileUser::find()->all(), 'id', 'login')) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'balance')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(\app\models\MUserWallet::getTypes()) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
