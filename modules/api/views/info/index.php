Получение ЧАВО<br>
api/faq/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","subject","content","views_count","created_at"}, [...], ...]<br>
<br>
Получение новостей<br>
api/news/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","subject","content","views_count","created_at"}, [...], ...]<br>
<br>
Получение уведомлений<br>
api/notification/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","subject","content","views_count","created_at"}, [...], ...]<br>
<br>
Регистрация пользователя<br>
api/user/register<br>
[<span style="color: red">POST</span>]<br>
-login* (string)<br>
-password* (string)<br>
-email* (string)<br>
-name (string)<br>
-country (string)<br>
ОТДАЕТ: токен для авторизации (string)<br>
<br>
Изменить пользователя<br>
api/user/edit<br>
[<span style="color: red">POST</span>]<br>
-name (string)<br>
-country (string)<br>
-email (string)<br>
ОТДАЕТ: Объект<br>
<br>
Загрузить фото пользователю<br>
api/user/upload-photo<br>
[<span style="color: red">POST</span>]<br>
-file (File)<br>
-type (int)<br>
ОТДАЕТ: Объект<br>
<br>
Получить список типов изображений<br>
api/user/photo-list<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ: Ключ — ID типа, Значение — Наименование типа<br>
<br>
Получить список стран<br>
api/user/countries-list<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ: Ключ — Код страны, Значение — Наименование страны<br>
<br>
Востанавление пароля<br>
Отправляет новый пароль на почту (на email)<br>
api/user/send-forget-password<br>
[<span style="color: red">POST</span>]<br>
-email* (string)<br>
{result: true}<br>
<br>
Установить FCM-токен<br>
api/user/set-fcm-token<br>
[<span style="color: red">POST</span>]<br>
-fcm_token* (string)<br>
{result: true}<br>
<br>
Изменить пароль<br>
api/user/change-password<br>
[<span style="color: red">POST</span>]<br>
-token* (string)<br>
-oldPassword* (string)<br>
-newPassword* (string)<br>
{result: true}<br>
<br>
Получение токена<br>
api/user/get-token<br>
[<span style="color: red">POST</span>]<br>
-login* (string)<br>
-password* (string)<br>
ОТДАЕТ: токен для авторизации (string)<br>
Отправить заявку на востановление пароля<br>
api/user/send-forget-password<br>
[<span style="color: red">GET</span>]<br>
-login* (string)<br>
ОТДАЕТ: {result: true} | Отправиться на почту сообщение с ссылкой<br>
<br>
Создание тикета<br>
api/ticket/create<br>
[<span style="color: green">POST</span>]<br>
-subject* (int) Заголовок тикета<br>
-description* (int) Содержание проблемы<br>
ОТДАЕТ:<br>
{Ticket}<br>
Получить тикеты<br>
api/ticket/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
{Ticket, Ticket, ...}<br>
Статус тикета храниться в свойстве status (integer)<br>
Возможные статусы:<br>
0 — Новый<br>
1 — В работе<br>
2 — Завершен<br>
3 — Отклонен<br>
<br>
Получение сообщений тикета<br>
api/ticket/get-messages<br>
[<span style="color: green">GET</span>]<br>
-postId* (int) ID тикета<br>
ОТДАЕТ:<br>
[{"id","ticket_id","text","from","created_at"}, [...], ...]<br>
<br>
Отправить сообщение<br>
api/ticket/send-message<br>
[<span style="color: red">POST</span>]<br>
-postId* (int) ID тикета<br>
-text* (string)<br>
ОТДАЕТ:<br>
true|false<br>
<br>
Получить кошельки<br>
api/wallet/index<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
[{"id","mobile_user_id","token","password","balance","created_at","type"}, [...], ...]<br>
"type" — Валюта. Возможные значения: (bitcoin, litecoin, ethereum)<br>
<br>
Создать кошелек<br>
api/wallet/index<br>
[<span style="color: green">POST</span>]<br>
string password* | Пароль кошелька<br>
string label | Наименование кошелька<br>
ОТДАЕТ:<br>
{"guid", "address", "label"}<br>
<br>
Получить транзакции<br>
api/transaction/index<br>
[<span style="color: green">GET</span>]<br>
Возможные параметры для фильтров:<br>
string period_from, string period_to: Фильтр "Период", Заполняются два параметра сразу в формате (Y-m-d H:i:s)<br>
string currency: Фильтр "Валюта", Возможные значения: (bitcoin, litecoin, ethereum)<br>
string type: Фильтр "Тип операции", Возможные значения: (Покупка — buy, Перевод — transfer, Вывод — out)<br>
ОТДАЕТ:<br>
[{"id","mobile_user_id","wallet_from_id","wallet_to","type","amount","commission","status","comment","created_at", "currency", "key"}, [...], ...]<br>
Вариант ссылки по ключу: https://www.blockchain.com/btc/tx/1ae2ecf8f1d0aebc839549aa580f0d6b55c72a1cbb1807642ebd8836551985bb<br>
Статусы транзакций:<br>
0 - не проведена<br>
1 - проведена<br>
<br>
Получить Курсы валют<br>
api/transaction/rates<br>
[<span style="color: green">GET</span>]<br>
ОТДАЕТ:<br>
{<br>
"btc":{<br>
"valueBuy":649529.48657, | Курс BTCRUB с учетом комисси за покупку<br>
"valueTransfer":649529.48657, | Курс BTCRUB с учетом комисси за перевод<br>
"baseBalance":4.99994 | Баланс BTC системы<br>
},<br>
<!--    "ltc":{<br>-->
<!--        "value":4791.1763449725695, | Курс BTCLTC<br>-->
<!--        "baseBalance":3.6331 | Баланс LTC системы<br>-->
<!--    },<br>-->
<!--    "eth":{<br>-->
<!--        "value":14036.4462899124, | Курс BTCETH<br>-->
<!--        "baseBalance":3.92284 | Баланс ETH системы<br>-->
<!--    },<br>-->
<!--    "commission":{<br>-->
<!--        "buyPercent":0, | Процентная комиссия за покупку<br>-->
<!--        "transactionPercent":0, | Процентная комиссия за перевод<br>-->
<!--        "buyValue":0, | Комиссия за покупку в значении<br>-->
<!--        "transactionValue":0 | Комиссия за перевод в значении<br>-->
<!--    },<br>-->
"max_rub":{<br>
"buyValue":100, | Максимальное значение покупки в рублях
}<br>
"min_rub":{<br>
"buyValue":100, | Минимальное значение покупки в рублях<br>
"transferValue":100, | Минимальное значение перевод в рублях<br>
"commission":100 | Минимальное значение коммисси в рублях<br>
}<br>
}<br>
<br>
Создать транзакцию<br>
api/transaction/create<br>
[<span style="color: red">POST</span>]<br>
int $wallet_from_id Кошелек отправителя (ID вашего кошелька)<br>
string $wallet_to Кошелек получателя<br>
string $type Тип операции ( 'buy' — покупка, 'transfer' — перевод, 'out' — Вывод )<br>
double $amount Сумма<br>
double $commission Комиссия<br>
string $comment Комментарий<br>
ОТДАЕТ:<br>
[{"id","mobile_user_id","wallet_from_id","wallet_to","type","amount","commission","status","comment","created_at"}, [...], ...]<br>