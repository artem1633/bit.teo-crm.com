<?php

use yii\db\Migration;

/**
 * Handles adding guid to table `m_user_wallet`.
 */
class m190914_012843_add_guid_column_to_m_user_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('m_user_wallet', 'guid', $this->string()->after('mobile_user_id')->comment('Blockchain Wallet identifier'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('m_user_wallet', 'guid');
    }
}
