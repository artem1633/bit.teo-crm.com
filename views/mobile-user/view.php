<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $model app\models\MobileUser */

$btcBalance = $btcWallet != null ? $btcWallet->balance : 0;
$ltcBalance = $ltcWallet != null ? $ltcWallet->balance : 0;
$ethBalance = $ethWallet != null ? $ethWallet->balance : 0;

CrudAsset::register($this);

?>
<div class="mobile-user-view">

    <div class="row">
        <div class="col-md-9">
            <?= $this->render('@app/views/transaction/index', [
                'searchModel' => $transactionSearch,
                'dataProvider' => $transactionDataProvider,
            ]) ?>
            <?= $this->render('@app/views/ticket/index', [
                'searchModel' => $ticketSearch,
                'dataProvider' => $ticketDataProvider,
            ]) ?>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Информация
                    </h4>
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'login',
                            'password',
                            'email:email',
                            'created_at',
                            'last_login_datetime',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Файлы
                    </h4>
                </div>
                <div class="panel-body">
                    <?= $this->render('@app/views/mobile-user-file/index', [
                        'searchModel' => $filesSearchModel,
                        'dataProvider' => $filesDataProvider,
                    ])  ?>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Кошелек BTC
                    </h4>
                    <div class="heading-btn pull-right">
                        <?= Html::a('<i class="fa fa-pencil"></i>', ['m-user-wallet/update', 'id' => $btcWallet->id], ['role' => 'modal-remote', 'class' => 'btn btn-xs btn-primary', 'style' => 'margin-top: -40px;']) ?>
                    </div>
                </div>
                <div class="panel-body">
                    <h3><?=number_format($btcBalance, 8, '.', ' ')?> BTC</h3>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Кошелек LTC
                    </h4>
                    <div class="heading-btn pull-right">
                        <?= Html::a('<i class="fa fa-pencil"></i>', ['m-user-wallet/update', 'id' => $btcWallet->id], ['role' => 'modal-remote', 'class' => 'btn btn-xs btn-primary', 'style' => 'margin-top: -40px;']) ?>
                    </div>
                </div>
                <div class="panel-body">
                    <h3><?=number_format($ltcBalance, 8, '.', ' ')?> BTC</h3>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Кошелек ETH

                    </h4>
                    <div class="heading-btn pull-right">
                        <?= Html::a('<i class="fa fa-pencil"></i>', ['m-user-wallet/update', 'id' => $btcWallet->id], ['role' => 'modal-remote', 'class' => 'btn btn-xs btn-primary', 'style' => 'margin-top: -40px;']) ?>
                    </div>
                </div>
                <div class="panel-body">
                    <h3><?=number_format($ethBalance, 8, '.', ' ')?> BTC</h3>
                </div>
            </div>
        </div>
    </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
