<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $mobile_user_id Пользователь
 * @property string $subject Заголовок
 * @property string $description Описание
 * @property string $status Статус
 * @property int $is_read Прочитано
 * @property string $last_message_datetime Дата и время последнего сообщения
 * @property string $created_at Дата и время создания
 * @property string $closed_datetime Дата и время закрытия
 *
 * @property MobileUser $mobileUser
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_WORK = 1;
    const STATUS_DONE = 2;
    const STATUS_REJECTED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile_user_id', 'is_read'], 'integer'],
            [['description'], 'string'],
            [['last_message_datetime', 'created_at', 'closed_datetime'], 'safe'],
            [['subject', 'status'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_NEW],
            [['mobile_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['mobile_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile_user_id' => 'Пользователь',
            'subject' => 'Заголовок',
            'description' => 'Описание',
            'status' => 'Статус',
            'is_read' => 'Прочитано',
            'last_message_datetime' => 'Дата и время последнего сообщения',
            'created_at' => 'Дата и время создания',
            'closed_datetime' => 'Дата и время закрытия',
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнено',
            self::STATUS_REJECTED => 'Отклонен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobileUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'mobile_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return boolean
     */
    public function updateLastMessageDateTime($datetime = null)
    {
        if($datetime != null){
            $this->last_message_datetime = $datetime;
        } else {
            $this->last_message_datetime = date('Y-m-d H:i:s');
        }

        return $this->save(false);
    }
}
