<?php

namespace app\models;

use app\services\CurrencyConverter;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $mobile_user_id Пользователь
 * @property int $wallet_from_id Кошелек отправитель
 * @property string $wallet_to Кошелек получателя
 * @property string $type Тип операции
 * @property double $amount Сумма
 * @property double $amount_rub Сумма в рублях
 * @property double $commission Комиссия
 * @property string $status Статус
 * @property string $comment Комментарий
 * @property int $verify
 * @property string $created_at Дата и время создания
 * @property string $key Ключ
 * @property string $tx_hash Хэш
 *
 * @property MobileUser $mobileUser
 * @property MUserWallet $walletFrom
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TYPE_BUY = 'buy';
    const TYPE_TRANSFER = 'transfer';

    /**
     * @depricated
     */
    const TYPE_OUT = 'out';

    public $strictBtc;

    private $amountDefault;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile_user_id', 'wallet_from_id', 'type'], 'required'],
            ['wallet_to', 'required', 'when' => function(){
                return $this->type != self::TYPE_BUY;
            }],
            [['mobile_user_id', 'wallet_from_id'], 'integer'],
            [['amount', 'commission', 'amount_rub', 'strictBtc'], 'number'],
            [['comment', 'key'], 'string'],
            [['created_at'], 'safe'],
            [['wallet_to', 'type', 'status', 'tx_hash'], 'string', 'max' => 255],
            [['mobile_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['mobile_user_id' => 'id']],
            [['wallet_from_id'], 'exist', 'skipOnError' => true, 'targetClass' => MUserWallet::className(), 'targetAttribute' => ['wallet_from_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->amount = number_format($this->amount, 11, '.', '');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        // echo $this->amount_rub;
        // exit;

        // $this->amount = $this->amount_rub / 100000000;

        // $this->amount_rub = (new CurrencyConverter($this->amount))->convertBTCRUB();
        if($this->type == self::TYPE_TRANSFER){

            
        } else {
            if($this->strictBtc == null){
                $this->amount = (new CurrencyConverter($this->amount_rub))->convertRUBBTC();
            } else {
                $this->amount = $this->strictBtc;
            }
        }

        if($insert){
            $this->status = '0';
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheridoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            $wallet = $this->walletFrom;
            if($this->type == self::TYPE_BUY){
                $result = $wallet->replenish($this->amount);
                // $result = [];

                if(isset($result['tx_hash'])){
                    $this->key = $result['tx_hash'];
                } else {
                    $this->key = '';
                }
                $this->save();

                // if(count($result) > 0){
                // $this->save(false);
                // }
                // var_dump($result);
            } else if($this->type == self::TYPE_TRANSFER){
                // if($wallet->balance >= $this->amount){

                // if($wallet->balance < $this->amount){
                //     return false;
                // }

                // $result = $wallet->withdraw($this->amount, $this->wallet_to);
                // // $result = [];

                // if(isset($result['tx_hash'])){
                //     $this->key = $result['tx_hash'];
                // } else {
                //     $this->key = 'Не проведена';
                // }
                // $this->save();
                // if(count($result) > 0){
                // $this->save(false);
                // }
                // }
            } else if($this->type == self::TYPE_OUT){
                // $wallet->withdraw($this->amount);
            }
        }

        $wallet = $this->walletFrom;

        if($this->type == self::TYPE_TRANSFER && $this->status == '1'){

            if($wallet->balance < $this->amount){
                return false;
            }

            $result = $wallet->withdraw($this->amount, $this->wallet_to);

            if(isset($result['tx_hash'])){
                $this->key = $result['tx_hash'];
                $this->status == '2';
            } else {
                $this->key = 'Не проведена';
                $this->status == '4';
            }
            $this->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile_user_id' => 'Пользователь',
            'wallet_from_id' => 'Кошелек отправитель',
            'wallet_to' => 'Кошелек получателя',
            'key' => 'Ключ',
            'type' => 'Тип операции',
            'amount' => 'Сумма',
            'tx_hash' => 'Хэш',
            'amount_rub' => 'Сумма в рублях',
            'commission' => 'Комиссия',
            'status' => 'Статус',
            'comment' => 'Комментарий',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_BUY => 'Покупка',
            self::TYPE_TRANSFER => 'Перевод',
            self::TYPE_OUT => 'Вывод',
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            '0' => 'Новый',
            '1' => 'Разрешен',
            '2' => 'Выполнено',
            '3' => 'Откланен',
            '4' => 'Не проведена'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobileUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'mobile_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletFrom()
    {
        return $this->hasOne(MUserWallet::className(), ['id' => 'wallet_from_id']);
    }
}
