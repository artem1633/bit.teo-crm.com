<?php

namespace app\components;

use app\components\blockchain\BlockchainAccount;
use app\models\Settings;
use Yii;
use yii\base\Component;

/**
 * Class AdminBalance
 *
 * @property BlockchainAccount $blockchainAccount
 * @property double $rubRate Курс BTC к рублю
 * @property double $btcRate Курс BTC к рублю
 * @property double $ltcRate Курс LTC к рублю
 * @property double $ethRate Курс ETH к рублю
 */
class AdminBalance extends Component
{
    private $rub = null;
    private $btc = null;
    private $ltc = null;
    private $eth = null;

    /**
     * @return BlockchainAccount
     */
    public function getBlockchainAccount()
    {
        $adminGuid = Settings::findByKey('exchange_token')->value;
        $adminPassword = Settings::findByKey('exchange_password')->value;

        return new BlockchainAccount([
            'guid' => $adminGuid,
            'password' => $adminPassword,
        ]);
    }

    /**
     * @param double $amount
     */
    public function withdrawBtc($amount)
    {
        $base = Settings::findOne(['key' => 'btc_base_balance']);
        $value = doubleval($base->value) - $amount;
        $base->value = $value;
        $base->save(false);
    }

    /**
     * @param double $amount
     */
    public function withdrawLtc($amount)
    {
        $base = Settings::findOne(['key' => 'ltc_base_balance']);
        $value = doubleval($base->value) - $amount;
        $base->value = $value;
        $base->save(false);
    }

    /**
     * @param double $amount
     */
    public function withdrawEth($amount)
    {
        $base = Settings::findOne(['key' => 'eth_base_balance']);
        $value = doubleval($base->value) - $amount;
        $base->value = $value;
        $base->save(false);
    }

    /**
     * @return double
     */
    public function getRubRate()
    {
        if($this->rub == null){
            $this->rub = json_decode(file_get_contents('https://blockchain.info/ticker'), true)['RUB']['buy'];
        }

        return $this->rub;
    }

    /**
     * @return double
     */
    public function getBtcRate()
    {
        if($this->btc == null){
            // $this->btc = json_decode(file_get_contents('https://dsx.uk/mapi/trades/btcrub'), true)['btcrub'][0]['price'];
            $this->btc = json_decode(file_get_contents('https://blockchain.info/ticker'), true)['USD']['buy'];
        }

        return $this->btc;
    }

    /**
     * @return double
     */
    public function getLtcRate()
    {
        if($this->ltc == null){
            // $this->ltc = json_decode(file_get_contents('https://dsx.uk/mapi/trades/ltcusd'), true)['ltcusd'][0]['price'] * $this->getRubRate();
            // $this->ltc = json_decode(file_get_contents('https://api.dsxglobal.com/mapi/v2/trades/ltcusd'), true)['ltcusd'][0]['price'] * $this->getRubRate();
            $this->ltc = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym=LTC&tsyms=USD&limit=1'), true)['LTC']['USD'];
        }

        return $this->ltc;
    }

    /**
     * @return double
     */
    public function getEthRate()
    {
        if($this->eth == null){
            // $this->eth = json_decode(file_get_contents('https://dsx.uk/mapi/trades/ethusd'), true)['ethusd'][0]['price'] * $this->getRubRate();
            // $this->eth = json_decode(file_get_contents('https://api.dsxglobal.com/mapi/v2/trades/ethusd'), true)['ethusd'][0]['price'] * $this->getRubRate();
             $this->eth = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/pricehistorical?fsym=ETH&tsyms=USD&limit=1'), true)['ETH']['USD'];
        }

        return $this->eth;
    }
}