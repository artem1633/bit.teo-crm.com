<?php

use yii\db\Migration;

/**
 * Class m190920_144958_add_exchange_password_setting
 */
class m190920_144958_add_exchange_password_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'exchange_password',
            'value' => '',
            'label' => 'Пароль биржи',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'exchange_password']);
    }
}
