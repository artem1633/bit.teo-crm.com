<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Мобильные пользователи', 'icon' => 'fa  fa-mobile', 'url' => ['/mobile-user'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],],
                    ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'],],
                    ['label' => 'Доп. инфа', 'icon' => 'fa fa-book', 'url' => '#', 'options' => [
                        'class' => 'has-sub',
                    ], 'items' => [
                        ['label' => 'Транзакции', 'icon' => 'fa fa-ruble', 'url' => ['/transaction'],],
                        ['label' => 'Тикеты', 'icon' => 'fa  fa-pencil', 'url' => ['/ticket'],],
                        ['label' => 'Кошельки пользователей', 'icon' => 'fa  fa-mobile', 'url' => ['/m-user-wallet'],],
                        ['label' => 'Новости', 'icon' => 'fa  fa-th', 'url' => ['/news'],],
                        ['label' => 'ЧАВО', 'icon' => 'fa  fa-th', 'url' => ['/faq'],],
                        ['label' => 'Уведомления', 'icon' => 'fa  fa-th', 'url' => ['/notification'],],
                    ]],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
