<?php

namespace app\modules\api\controllers;

use app\components\AdminBalance;
use app\models\MobileUser;
use app\models\Settings;
use app\models\Transaction;
use app\models\TransactionSearch;
use app\models\MUserWallet;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use app\components\FcmPusher;
use app\models\User;

class TransactionController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionRates()
    {
        /** @var AdminBalance $adminBalance */
        $adminBalance = Yii::$app->adminBalance;
        $btc = $adminBalance->rubRate;
        $ltc = $adminBalance->ltcRate;
        $eth = $adminBalance->ethRate;

        $baseBtc = floatval(Settings::findByKey('btc_base_balance')->value);
        $baseLtc = floatval(Settings::findByKey('ltc_base_balance')->value);
        $baseEth = floatval(Settings::findByKey('eth_base_balance')->value);
        $maxBuyRub = floatval(Settings::findByKey('max_buy_rub')->value);

        $commissionBuyPercent = floatval(Settings::findByKey('commission_buy_percent')->value);
        $commissionTransactionPercent = floatval(Settings::findByKey('commission_transaction_percent')->value);
        $commissionBuyValue = floatval(Settings::findByKey('commission_buy_value')->value);
        $commissionTransactionValue = floatval(Settings::findByKey('commission_transaction_value')->value);

//        'min_transaction_buy_value_rub', 'min_transaction_commission_rub', 'min_transaction_transfer_value_rub'

        $minBuyValue = floatval(Settings::findByKey('min_transaction_buy_value_rub')->value);
        $minTransferValue = floatval(Settings::findByKey('min_transaction_transfer_value_rub')->value);
        $minCommission = floatval(Settings::findByKey('min_transaction_transfer_value_rub')->value);


        // $btc / $commissionBuyPercent;

//         return [
//             // 'btc' => [
//             //     'valueBuy' => $btc + ($btc / 100 * $commissionBuyPercent) + $commissionBuyValue,
//             //     'valueTransfer' => $btc + ($btc / 100 * $commissionTransactionPercent) + $commissionTransactionValue,
//             //     'baseBalance' => $baseBtc,
//             // ],
//             'btc' => [
//                 'valueBuy' => $btc + ($btc / 100 * $commissionBuyPercent) + $commissionBuyValue,
//                 'valueTransfer' => $btc + ($btc / 100 * $commissionTransactionPercent) + $commissionTransactionValue,
//                 'baseBalance' => $baseBtc,
//             ],
// //            'ltc' => [
// //                'value' => $ltc,
// //                'baseBalance' => $baseLtc,
// //            ],
// //            'eth' => [
// //                'value' => $eth,
// //                'baseBalance' => $baseEth,
// //            ],
//            'commission' => [
//                'buyPercent' => $commissionBuyPercent,
//                'transactionPercent' => $commissionTransactionPercent,
//                'buyValue' => $commissionBuyValue,
//                'transactionValue' => $commissionTransactionValue
//            ],
//             'max_rub' => [
//                 'buyValue' => $maxBuyRub,
//             ],
//             'min_rub' => [
//                 'buyValue' => $minBuyValue,
//                 'transferValue' => $minTransferValue,
//                 'commission' => $minCommission
//             ],
//         ];

        // var_dump($ltc);
        // var_dump($eth);
        // exit;

        return [
            // 'btc' => [
            //     'valueBuy' => $btc + ($btc / 100 * $commissionBuyPercent) + $commissionBuyValue,
            //     'valueTransfer' => $btc + ($btc / 100 * $commissionTransactionPercent) + $commissionTransactionValue,
            //     'baseBalance' => $baseBtc,
            // ],
            'btc' => [
                'valueBuy' => round($btc + ($btc / 100 * $commissionBuyPercent) + $commissionBuyValue),
                'valueTransfer' => $btc + ($btc / 100 * $commissionTransactionPercent) + $commissionTransactionValue,
                'baseBalance' => $baseBtc,
            ],
           // 'ltc' => [
           //     'value' => $ltc,
           //     'baseBalance' => $baseLtc,
           // ],
           // 'eth' => [
           //     'value' => $eth,
           //     'baseBalance' => $baseEth,
           // ],
           'commission' => [
               'buyPercent' => $commissionBuyPercent,
               'transactionPercent' => $commissionTransactionPercent,
               'buyValue' => $commissionBuyValue,
               'transactionValue' => $commissionTransactionValue
           ],
            'max_rub' => [
                'buyValue' => $maxBuyRub,
            ],
            'min_rub' => [
                'buyValue' => $minBuyValue,
                'transferValue' => $minTransferValue,
                'commission' => $minCommission
            ],
        ];
    }


    /**
     *
     */
    public function actionIndex()
    {
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->searchApi(['model' => Yii::$app->request->queryParams]);
        $dataProvider->query->andWhere(['transaction.mobile_user_id' => $this->user->id]);

        return $dataProvider->models;
    }


	public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Transaction();
        $data = ['model' => $request->post()];
        $model->mobile_user_id = $this->user->id;

        // if($model->type == null){
        if(isset($data['model']['type']) == false || $data['model']['type'] == null){
            $data['model']['type'] = Transaction::TYPE_BUY;
        	$model->type = Transaction::TYPE_BUY;
        	$data['model']['amount_rub'] = $data['model']['amount'];
            $model->strictBtc = $data['model']['btc'];
        	$data['model']['amount'] = null;
        }


        if($model->wallet_from_id == null){
        	$walletFrom = MUserWallet::find()->where(['mobile_user_id' => $this->user->id, 'type' => [MUserWallet::TYPE_BTC, 'bitcoin']])->one();
        	if($walletFrom){
        		$model->wallet_from_id = $walletFrom->id;
        	}
        }

        if($model->load($data, 'model') && $model->save())
        {
            if($model->type == Transaction::TYPE_BUY){
                FcmPusher::push($this->user->fcm_token, 'Вы купили BTC', ['type' => FcmPusher::TYPE_FORM, 'body' => '']);
            }

            if($model->type == Transaction::TYPE_TRANSFER){
				$users = User::find()->where(['role' => 1])->andWhere(['is not', 'fcm_token', null])->all();
	            foreach($users as $user)
	            {
	                if($user){
	                    if($user->fcm_token){
	                        FcmPusher::push($user->fcm_token, 'Новая транзакция', [], false);   
	                    }
	                }
	            }
            }


            return $model;
        } else {
            return $model->errors;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

}