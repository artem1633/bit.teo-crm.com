<?php

use yii\db\Migration;

/**
 * Handles adding address to table `m_user_wallet`.
 */
class m190914_170532_add_address_column_to_m_user_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('m_user_wallet', 'address', $this->string()->after('mobile_user_id')->comment('Адрес кошелька'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('m_user_wallet', 'address');
    }
}
