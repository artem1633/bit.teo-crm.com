<?php

use yii\db\Migration;

/**
 * Handles adding phone to table `user`.
 */
class m201107_083845_add_phone_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'phone', $this->string()->comment('Номер телефона'));
        $this->addColumn('user', 'token', $this->string()->comment('Токен'));
        $this->addColumn('user', 'fcm_token', $this->string()->comment('FCM Токен'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'token');
        $this->dropColumn('user', 'fcm_token');
    }
}
