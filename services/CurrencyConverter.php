<?php

namespace app\services;

use Yii;
use app\components\AdminBalance;
use yii\base\Component;

/**
 * Class CurrencyConverter
 * @package app\services
 */
class CurrencyConverter extends Component
{
    /**
     * @var double
     */
    public $amount;

    /**
     * CurrencyConverter constructor.
     * @param double $amount
     * @param array $config
     */
    public function __construct($amount, array $config = [])
    {
        $this->amount = $amount;
        parent::__construct($config);
    }

    /**
     * @return double
     */
    public function convertBTCRUB()
    {
        /** @var AdminBalance $adminBalance */
        $adminBalance = Yii::$app->adminBalance;

        return $adminBalance->btcRate * $this->amount;
    }
}