<?php

use yii\db\Migration;

/**
 * Handles dropping token from table `m_user_wallet`.
 */
class m190914_170254_drop_token_column_from_m_user_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('m_user_wallet', 'token');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('m_user_wallet', 'token', $this->string()->after('mobile_user_id')->comment('Токен'));
    }
}
