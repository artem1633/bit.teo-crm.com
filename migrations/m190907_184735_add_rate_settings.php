<?php

use app\models\Settings;
use yii\db\Migration;

/**
 * Class m190907_184735_add_rate_settings
 */
class m190907_184735_add_rate_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'btc_rate',
            'label' => 'Курс BTC',
        ]);

        $this->insert('settings', [
            'key' => 'ltc_rate',
            'label' => 'Курс LTC',
        ]);

        $this->insert('settings', [
            'key' => 'eth_rate',
            'label' => 'Курс ETH',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Settings::deleteAll(['key' => ['btc_rate', 'ltc_rate', 'eth_rate']]);
    }
}
