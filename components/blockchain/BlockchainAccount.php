<?php

namespace app\components\blockchain;

use yii\base\Component;
use yii\base\InvalidConfigException;
use app\models\Settings;

/**
 * Class BlockchainAccount
 * @package app\components
 *
 * @property float $balance
 */
class BlockchainAccount extends Component
{
    /**
     * @var string
     */
    public $guid;

    /**
     * @var string
     */
    public $password;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->guid == null || $this->password == null)
        {
            throw new InvalidConfigException('login and password must be required');
        }
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        $ch = curl_init();
        $url = "http://localhost:3001/merchant/{$this->guid}/balance?password={$this->password}";

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch), true);

        curl_close($ch);

        if(isset($result['balance'])){
        	$balance = floatval($result['balance']);
        } else {
        	$balance = 0;
        	\Yii::warning($result, 'balance not found');
        }
       

        return $balance;
    }

/**
     * @param string $to
     * @param double $amount
     * @param string $from
     * @return array
     */
    public function payment($to, $amount, $from = null)
    {
        $ch = curl_init();
        $str = null;

        $amount = round($amount);

        // if($from == null){

            // $wallets = MUserWallet::find()->where(['guid' => $this->guid])->all();
            // $addressFrom = null;
            // foreach ($wallets as $wallet) {
                // if(($wallet->balance * 100000000) >= $amount){
                    // $addressFrom = $wallet->address;
                // }
            // }

            // $amount = 2000;

        	$guid = Settings::findByKey('exchange_token')->value;
        	$password = Settings::findByKey('exchange_password')->value;

            // $str = "http://localhost:3001/merchant/bcf6ef1b-cb6a-4659-975a-2c91f52e1426/payment?password=Q;28bJ7b&from=1&to=$to&amount=".$amount;
            $str = "http://localhost:3001/merchant/{$guid}/payment?password={$password}&from=1&to=$to&amount=".$amount;

            // for ($i=0; $i < 3; $i++) { 
                // $str = "http://localhost:3000/merchant/{$this->guid}/payment?password={$this->password}&from={$i}&to=$to&amount=".$amount;
            // }

            // $str = "http://localhost:3000/merchant/{$this->guid}/sendmany?password={$this->password}&fee=2000&recipients={}";
            // $str = "http://localhost:3000/merchant/{$this->guid}/sendmany?password={$this->password}&fee=2000&recipients={}";
        // } else {
            // $amount = 2000;

            // $str = "http://localhost:3000/merchant/{$this->guid}/payment?password={$this->password}&from=1&to=$to&amount=".$amount;

            // echo "http://localhost:3000/merchant/{$this->guid}/payment?password={$this->password}&from=0&to=$to&amount=".$amount;
            // exit;
        // }

        // $str = "http://localhost:3000/merchant/eee00596-141e-49ec-b99f-415300b5530c/payment?password=4sOpnKquCl&from=0&to=12FmHjvPkKnn6eWqjyW7qB8KSbTCKD5XRK&amount=15615";


        // echo '<b>'.$amount.'</b>';
        // exit;

            \Yii::warning($str, 'String request address');

        if($str){
            // установка URL и других необходимых параметров
            curl_setopt($ch, CURLOPT_URL, $str);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // загрузка страницы и выдача её браузеру
            $result = json_decode(curl_exec($ch), true);

            // var_dump($result);

            \Yii::warning($result, 'Result of blockchain service wallet API');

            // завершение сеанса и освобождение ресурсов
            curl_close($ch);

            return $result;
        }

        return [];
    }
}