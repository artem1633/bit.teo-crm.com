<?php

use app\models\Settings;
use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                <span class="navbar-logo"></span>
                <?=Yii::$app->name?>
            </a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <?php if(Yii::$app->user->isGuest == false): ?>
            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown navbar-admin">
                    <a id="btn-dropdown_admin" onclick="$(this).parent().find('ul').slideToggle('fast');" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->login?></span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li class="arrow"></li>
                        <li> <?= Html::a('Сменить пароль', ['site/reset-password'], ['role' => 'modal-remote', 'data-pjax' => 1]) ?> </li>
                        <li class="divider"></li>
                        <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown navbar-rate">
                    <a id="btn-dropdown_rate" onclick="$(this).parent().find('ul').slideToggle('fast');" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">Курсы</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li class="arrow"></li>
                        <li><a href="#">BTC: <?=Yii::$app->adminBalance->getBtcRate()?></a></li>
                        <li><a href="#">ETH: <?=Yii::$app->adminBalance->getEthRate()?></a></li>
                        <li><a href="#">LTC: <?=Yii::$app->adminBalance->getLtcRate()?></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown navbar-user">
                    <a id="btn-dropdown_user" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs"><?php

                                    $adminGuid = Settings::findByKey('exchange_token')->value;
                                    $adminPassword = Settings::findByKey('exchange_password')->value;

                                    if($adminGuid != null && $adminPassword != null){
                                        echo strval(Yii::$app->adminBalance->blockchainAccount->balance / 100000000).' BTC ';
                                        $btc = Yii::$app->adminBalance->getBtcRate();
                                        echo '('.strval(round($btc * (Yii::$app->adminBalance->blockchainAccount->balance / 100000000), 2)).' руб)';
                                    }
                                    ?></span>
                    </a>
                </li>
            </ul>
            <!-- end header navigation right -->
        <?php endif; ?>
    </div>
    <!-- end container-fluid -->
</div>