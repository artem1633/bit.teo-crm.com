<?php

use yii\db\Migration;

/**
 * Handles adding new to table `mobile_user`.
 */
class m191218_111100_add_new_columns_to_mobile_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('mobile_user', 'name', $this->string()->after('login')->comment('ФИО'));
        $this->addColumn('mobile_user', 'country', $this->string()->after('name')->comment('Страна'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('mobile_user', 'name');
        $this->dropColumn('mobile_user', 'country');
    }
}
