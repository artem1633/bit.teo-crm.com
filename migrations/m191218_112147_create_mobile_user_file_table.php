<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mobile_user_file`.
 */
class m191218_112147_create_mobile_user_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mobile_user_file', [
            'id' => $this->primaryKey(),
            'mobile_user_id' => $this->integer()->comment('Пользователь'),
            'name' => $this->string()->comment('Наименование файла'),
            'path' => $this->string()->comment('Путь'),
            'type' => $this->integer()->comment('Тип'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-mobile_user_file-mobile_user_id',
            'mobile_user_file',
            'mobile_user_id'
        );

        $this->addForeignKey(
            'fk-mobile_user_file-mobile_user_id',
            'mobile_user_file',
            'mobile_user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-mobile_user_file-mobile_user_id',
            'mobile_user_file'
        );

        $this->dropIndex(
            'idx-mobile_user_file-mobile_user_id',
            'mobile_user_file'
        );

        $this->dropTable('mobile_user_file');
    }
}
