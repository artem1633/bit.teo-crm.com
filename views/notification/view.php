<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */
?>
<div class="notification-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'subject',
            'content:ntext',
            'views_count',
            'created_at',
        ],
    ]) ?>

</div>
