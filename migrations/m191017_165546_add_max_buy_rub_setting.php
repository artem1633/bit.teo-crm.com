<?php

use yii\db\Migration;

/**
 * Class m191017_165546_add_max_buy_rub_setting
 */
class m191017_165546_add_max_buy_rub_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'max_buy_rub',
            'label' => 'Максимальная покупка в руб',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'max_buy_rub']);
    }
}
