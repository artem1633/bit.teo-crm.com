<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaction;
use yii\db\Query;

/**
 * TransactionSearch represents the model behind the search form about `app\models\Transaction`.
 */
class TransactionSearch extends Transaction
{

    /**
     * @var string Валюта
     */
    public $currency;

    /**
     * @var string Период (дата начала)
     */
    public $period_from;

    /**
     * @var string Период (дата окончания)
     */
    public $period_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mobile_user_id', 'wallet_from_id'], 'integer'],
            [['wallet_to', 'type', 'status', 'comment', 'currency', 'created_at', 'period_from', 'period_to', 'key'], 'safe'],
            [['amount', 'commission'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mobile_user_id' => $this->mobile_user_id,
            'wallet_from_id' => $this->wallet_from_id,
            'amount' => $this->amount,
            'commission' => $this->commission,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'wallet_to', $this->wallet_to])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchApi($params)
    {
        $query = new Query();

        $query->from('transaction');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $query->select('transaction.id, transaction.mobile_user_id, wallet_from_id, wallet_to, transaction.type, status, comment, m_user_wallet.type as currency, transaction.created_at, amount, commission, key');

        $this->load($params, 'model');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('m_user_wallet', 'transaction.wallet_from_id = m_user_wallet.id');

        $query->andFilterWhere([
            'transaction.id' => $this->id,
            'transaction.mobile_user_id' => $this->mobile_user_id,
            'wallet_from_id' => $this->wallet_from_id,
            'amount' => $this->amount,
            'transaction.type' => $this->type,
            'm_user_wallet.type' => $this->currency,
            'commission' => $this->commission,
            'transaction.created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'wallet_to', $this->wallet_to])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        $query->andFilterWhere(['between', 'transaction.created_at', $this->period_from, $this->period_to]);

        return $dataProvider;
    }
}
