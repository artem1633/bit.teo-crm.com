<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip'])->hint('
        Пароль должен содержать:
        <ul>
            <li>только латинские буквы</li>
            <li>минимум 6 символов</li>
            <li>миним одну заглавную букву</li>
            <li>миним одну цифру</li>
        </ul>
    ') ?>

    <?= $form->field($model, 'role')->dropDownList([
            \app\models\User::ROLE_ADMIN => 'Админ',
            \app\models\User::ROLE_MANAGER => 'Менеджер',
    ]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
