<?php

use yii\db\Migration;

/**
 * Handles adding key to table `transaction`.
 */
class m191022_175940_add_key_column_to_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('transaction', 'key', $this->string()->comment('Ключ'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('transaction', 'key');
    }
}
