<?php

namespace app\modules\api\controllers;

use app\models\forms\UploadFileForm;
use app\models\MobileUserFile;
use Yii;
use app\models\MobileUser;
use app\models\PasswordForget;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

class UserController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new RegisterModel();

        if($model->load($data, 'model')) {

            $token = $model->register();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        $this->user->fcm_token = $token;
        $result = $this->user->save(false);

        return ['result' => $result];
    }

    /**
     * GET
     * @param string $email
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionSendForgetPassword($email)
    {
        $user = MobileUser::find()->where(['email' => $email])->one();

        if($user == null){
            throw new NotFoundHttpException();
        }

        $passwordForget = PasswordForget::find()->where(['m_user_id' => $user->id])->all();

        if($passwordForget == null){
            $passwordForget = PasswordForget::generate($user->id);
            $passwordForget->save(false);
            $result = Yii::$app->mailer->compose()
                ->setFrom('bit.notification.service@mail.ru')
                ->setTo($email)
                ->setSubject('Востановление пароля')
                ->setHtmlBody("Ваш временный пароль: {$passwordForget->new_password}")
                ->send();
            var_dump($result);
            $user->password = $passwordForget->new_password;
            $user->save(false);
        }

        return ['result' => true];
    }

    /**
     * POST
     */
    public function actionChangePassword()
    {
        $user = MobileUser::find()->where(['password' => $_POST['oldPassword']])->andWhere(['id' => $this->user->id])->one();

        if($user == null){
            return ['error' => 'Неверный пароль'];
        }

        $user->password = $_POST['newPassword'];
        $result = $user->save(false);

        return ['result' => $result];
    }

    public function actionEdit()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->user;

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }

    /**
     * @return mixed
     */
    public function actionCountriesList()
    {
        return MobileUser::getCountries();
    }


    /**
     * @return UploadFileForm|array
     */
    public function actionUploadPhoto()
    {
        $request = Yii::$app->request;

        // $_FILES['file']['name'] = 'file.png';
        // $_FILES['file']['type'] = 'image/png';
        // $_FILES['file']['tmp_name'] = '/tmp/phpgX8hF1';
        // $_FILES['file']['error'] = 0;
        // $_FILES['file']['size'] = 1839390;

        $data = ['model' => $request->post()];

        // $fileName = strval($_FILES['file']['name']);
        //      unset($_FILES['file']['name']);
        //      ArrayHelper::setValue($_FILES,'UploadFileForm.name.file', [0 => $fileName]);

        //      $fileType = strval($_FILES['file']['type']);
        //      unset($_FILES['file']['type']);
        //      ArrayHelper::setValue($_FILES,'UploadFileForm.type.file', [0 => $fileType]);

        //      $tmpFile = strval($_FILES['file']['tmp_name']);
        //      unset($_FILES['file']['tmp_name']);
        //      ArrayHelper::setValue($_FILES,'UploadFileForm.tmp_name.file', [0 => $tmpFile]);

        //      $fileError = intval($_FILES['file']['error']);
        //      unset($_FILES['file']['error']);
        //      ArrayHelper::setValue($_FILES,'UploadFileForm.error.file', [0 => $fileError]);

        //      $size = intval($_FILES['file']['size']);
        //      unset($_FILES['file']['size']);
        //      ArrayHelper::setValue($_FILES,'UploadFileForm.size.file', [0 => $size]);

        Yii::warning($_FILES);

        // $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'file' => $_FILES['UploadForm']]);

        $model = new UploadFileForm(['userId' => $this->user->id]);

        if($model->load($data, 'model') && $model->validate())
        {
            $model->upload();
            return $model;
        } else {
            return $model->errors;
        }
    }

    /**
     * @return array
     */
    public function actionPhotoList()
    {
        return MobileUserFile::getTypes();
    }


    /**
     * POST
     */
    public function actionGetToken()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new Login();

        if($model->load($data, 'model')) {

            $token = $model->login();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    public function beforeAction($action)
    {
        if(in_array($action->id, ['change-password', 'set-fcm-token', 'edit', 'upload-photo'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = MobileUser::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }
        }

        return parent::beforeAction($action);
    }
}