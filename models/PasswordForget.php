<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "password_forget".
 *
 * @property int $id
 * @property int $m_user_id Пользователь
 * @property string $token Токен для изменения пароля
 * @property int $status Статус
 * @property string $new_password Новый пароль
 * @property string $created_at
 *
 * @property MobileUser $mUser
 */
class PasswordForget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'password_forget';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_user_id', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['token', 'new_password'], 'string', 'max' => 255],
            [['m_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['m_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'm_user_id' => 'Пользователь',
            'token' => 'Токен для изменения пароля',
            'status' => 'Статус',
            'new_password' => 'Новый пароль',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * @param int $userId
     * @return $this
     */
    public static function generate($userId)
    {
        $model = new self([
            'm_user_id' => $userId,
            'token' => Yii::$app->security->generateRandomString(),
            'new_password' => Yii::$app->security->generateRandomString(7),
        ]);
        $model->save(false);

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'm_user_id']);
    }
}