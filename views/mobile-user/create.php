<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MobileUser */

?>
<div class="mobile-user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
