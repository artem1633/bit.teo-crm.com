<?php

use yii\db\Migration;

/**
 * Class m191017_164146_rename_settings
 */
class m191017_164146_rename_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\models\Settings::updateAll(['label' => 'Минимальная покупка в руб'], ['key' => 'min_transaction_buy_value_rub']);
        \app\models\Settings::updateAll(['label' => 'Минимальный перевод в биткоин'], ['key' => 'min_transaction_transfer_value_rub']);
        \app\models\Settings::updateAll(['label' => 'Комиссия за перевод биткоина в BTC'], ['key' => 'commission_transaction_value']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
