<?php

namespace app\modules\api\controllers;

use app\models\User;
use app\components\AdminBalance;
use app\models\MobileUser;
use app\models\Transaction;
use app\models\TransactionSearch;
use app\models\MUserWallet;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use app\components\FcmPusher;
use yii\web\BadRequestHttpException;
use app\models\TicketSearch;
use app\models\Ticket;
use app\models\TicketMessage;
use app\models\Settings;

class AdminController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionRates()
    {
        /** @var AdminBalance $adminBalance */
        $adminBalance = Yii::$app->adminBalance;
        $btc = $adminBalance->rubRate;
        // $ltc = $adminBalance->ltcRate;
        // $eth = $adminBalance->ethRate;

        $baseBtc = floatval(Settings::findByKey('btc_base_balance')->value);
        // $baseLtc = floatval(Settings::findByKey('ltc_base_balance')->value);
        // $baseEth = floatval(Settings::findByKey('eth_base_balance')->value);
        // $maxBuyRub = floatval(Settings::findByKey('max_buy_rub')->value);

        $commissionBuyPercent = floatval(Settings::findByKey('commission_buy_percent')->value);
        $commissionTransactionPercent = floatval(Settings::findByKey('commission_transaction_percent')->value);
        $commissionBuyValue = floatval(Settings::findByKey('commission_buy_value')->value);
        $commissionTransactionValue = floatval(Settings::findByKey('commission_transaction_value')->value);

//        'min_transaction_buy_value_rub', 'min_transaction_commission_rub', 'min_transaction_transfer_value_rub'

        // $minBuyValue = floatval(Settings::findByKey('min_transaction_buy_value_rub')->value);
        // $minTransferValue = floatval(Settings::findByKey('min_transaction_transfer_value_rub')->value);
        // $minCommission = floatval(Settings::findByKey('min_transaction_transfer_value_rub')->value);

        return [
            // 'btc' => [
            //     'valueBuy' => $btc + ($btc / 100 * $commissionBuyPercent) + $commissionBuyValue,
            //     'valueTransfer' => $btc + ($btc / 100 * $commissionTransactionPercent) + $commissionTransactionValue,
            //     'baseBalance' => $baseBtc,
            // ],
            'btc' => [
                'valueBuy' => round($btc + ($btc / 100 * $commissionBuyPercent) + $commissionBuyValue),
                'valueTransfer' => $btc + ($btc / 100 * $commissionTransactionPercent) + $commissionTransactionValue,
                'baseBalance' => $baseBtc,
            ],
           // 'ltc' => [
           //     'value' => $ltc,
           //     'baseBalance' => $baseLtc,
           // ],
           // 'eth' => [
           //     'value' => $eth,
           //     'baseBalance' => $baseEth,
           // ],
           // 'commission' => [
           //     'buyPercent' => $commissionBuyPercent,
           //     'transactionPercent' => $commissionTransactionPercent,
           //     'buyValue' => $commissionBuyValue,
           //     'transactionValue' => $commissionTransactionValue
           // ],
           //  'max_rub' => [
           //      'buyValue' => $maxBuyRub,
           //  ],
           //  'min_rub' => [
           //      'buyValue' => $minBuyValue,
           //      'transferValue' => $minTransferValue,
           //      'commission' => $minCommission
           //  ],
        ];
    }

    /**
     *
     */
    public function actionTransactions()
    {
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->searchApi(['model' => Yii::$app->request->queryParams]);
        $dataProvider->query->andWhere(['transaction.type' => 'transfer']);

        $models = (array) $dataProvider->models;

        array_walk($models, function(&$item){
        	$item['mobileUser'] = MobileUser::find()->where(['id' => $item['mobile_user_id']])->asArray()->one();

        	if($item['mobileUser']){
        		$item['mobileUser']['walletBtc'] = MUserWallet::find()->where(['mobile_user_id' => $item['mobile_user_id'], 'type' => MUserWallet::TYPE_BTC])->one();
        	}
        });

        return $models;
    }

    public function actionTickets()
    {
        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->asArray();
        $dataProvider->query->orderBy('id desc');

        $models = (array) $dataProvider->models;

        array_walk($models, function(&$item){
            $item['mobileUser'] = MobileUser::find()->where(['id' => $item['mobile_user_id']])->asArray()->one();
        });

        return $models;
    }

    public function actionTicketMessages()
    {
        if(isset($_GET['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        $model = Ticket::findOne($_GET['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $messages = TicketMessage::find()->where(['ticket_id' => $model->id])->all();

        return $messages;
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        $this->user->fcm_token = $token;
        $result = $this->user->save(false);

        return ['result' => $result];
    }


    /**
     * postId integer
     * text string
     */
    public function actionTicketSendMessage()
    {
        if(isset($_POST['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        if(isset($_POST['text']) == null){
            throw new BadRequestHttpException('text must be required');
        }

        /** @var Ticket $model */
        $model = Ticket::findOne($_POST['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $message = new TicketMessage([
            'ticket_id' => $model->id,
            'from' => TicketMessage::FROM_MANAGER,
            'text' => $_POST['text']
        ]);
        $result = $message->save(false);

        $model->updateLastMessageDateTime($message->created_at);

        if($result) {
            return $message;
        } else {
            return ['result' => false];
        }
    }

    /**
     * postId integer
     * text string
     */
    public function actionTicketChangeStatus()
    {
        if(isset($_POST['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        if(isset($_POST['status']) == null){
            throw new BadRequestHttpException('status must be required');
        }

        /** @var Ticket $model */
        $model = Ticket::findOne($_POST['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $model->status = $_POST['status'];
        $result = $model->save(false);

        if($result) {
            return $model;
        } else {
            return ['result' => false];
        }
    }

    public function actionTransactionApprove()
    {
         if(isset($_POST['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        /** @var Ticket $model */
        $model = Transaction::findOne($_POST['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $model->status = '1';
        $result = $model->save(false);

        if($result) {
            return $model;
        } else {
            return ['result' => false];
        }
    }

    public function actionTransactionDisapprove()
    {
         if(isset($_POST['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        /** @var Ticket $model */
        $model = Transaction::findOne($_POST['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $model->status = '3';
        $result = $model->save(false);

        if($result) {
            return $model;
        } else {
            return ['result' => false];
        }
    }

    public function actionGetToken()
    {
        if(isset($_POST['login']) == false || isset($_POST['password']) == false){
            throw new BadRequestHttpException('Не заполнены логин и пароль');
        }

        $login = $_POST['login'];
        $password = $_POST['password'];

        $token = md5($login).md5($password);

        $user = User::find()->where(['token' => $token])->one();

        if($user){
            return ['token' => $token, 'phone' => $user->phone];            
        } else {
            throw new BadRequestHttpException('Не верный логин и пароль');
        }

    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($action->id != 'get-token'){
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = User::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }
        }

        return parent::beforeAction($action);
    }

}