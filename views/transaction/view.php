<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
?>
<div class="transaction-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mobile_user_id',
            'wallet_from_id',
            'wallet_to',
            'type',
            'amount',
            'commission',
            'status',
            'comment:ntext',
            'created_at',
        ],
    ]) ?>

</div>
