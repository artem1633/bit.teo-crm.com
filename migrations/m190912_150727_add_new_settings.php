<?php

use yii\db\Migration;

/**
 * Class m190912_150727_add_new_settings
 */
class m190912_150727_add_new_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'btc_base_balance',
            'label' => 'Базовый баланс BTC',
            'value' => '0',
        ]);

        $this->insert('settings', [
            'key' => 'ltc_base_balance',
            'label' => 'Базовый баланс LTC',
            'value' => '0',
        ]);

        $this->insert('settings', [
            'key' => 'eth_base_balance',
            'label' => 'Базовый баланс ETH',
            'value' => '0',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => ['btc_base_balance', 'ltc_base_balance', 'eth_base_balance']]);
    }
}
