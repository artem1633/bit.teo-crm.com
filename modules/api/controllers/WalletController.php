<?php

namespace app\modules\api\controllers;

use app\models\Faq;
use app\models\MobileUser;
use app\models\MUserWallet;
use app\models\Transaction;
use app\models\Settings;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class WalletController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $models = MUserWallet::find()->where(['mobile_user_id' => $this->user->id, 'type' => MUserWallet::TYPE_BTC])->all();

        return $models;
    }

    public function actionUpdateAdminBalance()
    {
        $settingBalance = Settings::findByKey('btc_base_balance');
        $settingBalanceValue = floatval($settingBalance->value);

        $balance = Yii::$app->adminBalance->blockchainAccount->balance / 100000000;


        if($balance != $settingBalanceValue){
            $settingBalance->value = number_format($balance, 10);
            $settingBalance->save(false);
        }

        return [
            'balance' => $balance,
        ];
    }

    public function actionCommonBalance()
    {
        $model = MUserWallet::find()->where(['mobile_user_id' => $this->user->id, 'type' => MUserWallet::TYPE_BTC])->one();

        if($model == null){
            return 0;
        }

        $balance = $model->blockchainAccount->balance / 100000000;

        if($model->balance != $balance){
            $model->balance = $balance;
            $model->save(false);
        }

        return [
            'balance' => $model->blockchainAccount->balance,
        ];
    }

    public function actionCreate()
    {
        $password = $_POST['password'];
        $api_token = '4272f3cc-00da-4b61-832b-dae4d64fdef9';
        $label = $_POST['label'];
        $email = $this->user->email;

        $ch = curl_init('localhost:3001/api/v2/create');

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "password={$password}&api_code={$api_token}&label={$label}&email={$email}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $wallet = new MUserWallet([
            'mobile_user_id' => $this->user->id,
            'password' => $_POST['password'],
            'balance' => 0,
            'type' => MUserWallet::TYPE_BTC,
        ]);

        $wallet->save(false);

        $info = curl_exec($ch);

        curl_close($ch);

        return $info;
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null && $action->id != 'update-admin-balance'){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

}