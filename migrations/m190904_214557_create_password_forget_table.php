<?php

use yii\db\Migration;

/**
 * Handles the creation of table `password_forget`.
 */
class m190904_214557_create_password_forget_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('password_forget', [
            'id' => $this->primaryKey(),
            'm_user_id' => $this->integer()->comment('Пользователь'),
            'token' => $this->string()->comment('Токен для изменения пароля'),
            'new_password' => $this->string()->comment('Новый пароль'),
            'status' => $this->integer()->comment('Статус'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-password_forget-m_user_id',
            'password_forget',
            'm_user_id'
        );

        $this->addForeignKey(
            'fk-password_forget-m_user_id',
            'password_forget',
            'm_user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-password_forget-m_user_id',
            'password_forget'
        );

        $this->dropIndex(
            'idx-password_forget-m_user_id',
            'password_forget'
        );

        $this->dropTable('password_forget');
    }
}
