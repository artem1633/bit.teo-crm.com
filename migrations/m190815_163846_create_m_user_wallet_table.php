<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m_user_wallet`.
 */
class m190815_163846_create_m_user_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('m_user_wallet', [
            'id' => $this->primaryKey(),
            'mobile_user_id' => $this->integer()->comment('Мобильный пользователь'),
            'token' => $this->string()->comment('Токен'),
            'password' => $this->string()->comment('Пароль'),
            'balance' => $this->float()->comment('Баланс'),
            'created_at' => $this->dateTime()->comment('Дата и время создания'),
            'type' => $this->string()->comment('Тип валюты'),
        ]);

        $this->createIndex(
            'idx-m_user_wallet-mobile_user_id',
            'm_user_wallet',
            'mobile_user_id'
        );

        $this->addForeignKey(
            'fk-m_user_wallet-mobile_user_id',
            'm_user_wallet',
            'mobile_user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-m_user_wallet-mobile_user_id',
            'm_user_wallet'
        );

        $this->dropIndex(
            'idx-m_user_wallet-mobile_user_id',
            'm_user_wallet'
        );

        $this->dropTable('m_user_wallet');
    }
}
