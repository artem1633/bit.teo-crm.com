<?php

namespace app\models;

use app\components\AdminBalance;
use app\components\blockchain\BlockchainAccount;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "m_user_wallet".
 *
 * @property int $id
 * @property int $mobile_user_id Мобильный пользователь
 * @property string $guid Blockchain Wallet identifier
 * @property string $token Токен
 * @property string $password Пароль
 * @property double $balance Баланс
 * @property string $created_at Дата и время создания
 * @property string $type Тип валюты
 *
 * @property MobileUser $mobileUser
 * @property Transaction[] $transactions
 */
class MUserWallet extends \yii\db\ActiveRecord
{
    const START_BALANCE = 0;
    const TYPE_BTC = 'bitcoin';
    const TYPE_LTC = 'litecoin';
    const TYPE_ETH = 'ethereum';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_user_wallet';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile_user_id'], 'integer'],
            [['balance'], 'number'],
            [['created_at'], 'safe'],
            [['password', 'type', 'guid'], 'string', 'max' => 255],
            [['mobile_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['mobile_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->balance = number_format($this->balance, 11, '.', '');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile_user_id' => 'Мобильный пользователь',
            'password' => 'Пароль',
            'guid' => 'Blockchain Wallet identifier',
            'balance' => 'Баланс',
            'created_at' => 'Дата и время создания',
            'type' => 'Тип валюты',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            'BTC' => 'bitcoin',
            'LTC' => 'litecoin',
            'ETH' => 'ethereum',
        ];
    }

    /**
     * @return array
     */
    public function replenish($amount)
    {
        /** @var $setting Settings */
        $isDemo = Settings::getBooleanValueByKey('demo_transaction_mode');
        $adminGuid = Settings::findByKey('exchange_token')->value;
        $adminPassword = Settings::findByKey('exchange_password')->value;

        if($adminGuid == null || $adminPassword == null){
            return false;
        }
        $oldBalance = $this->balance;

        $this->balance += $amount;

        $result = [];

        if($isDemo == false){

            /** @var AdminBalance $adminBlance */
            $adminBalance = Yii::$app->adminBalance;
            $adminAccount = $adminBalance->getBlockchainAccount();
            $adminBalance->withdrawBtc($amount);

            $amountSatoshi = $amount * 100000000;

            // $result = $adminAccount->payment($this->address, $amountSatoshi, '1');
            // $result = json_decode('{"to":["19HnoZpLZV3Krn2kHRmLZmUW6J2NWf6i6h"],"amounts":[1104],"from":["xpub6DEE5k4gyw3TmJF17W6AoAPBFprpo6ayRLeRMSvtFN1uh9aynfFroXTi8Ctx1zYxS5wFftmvcANHZHXD6jpGMTx6tyCaQ38oN8ivae4QiyR"],"fee":1000,"txid":"22135352edeeaaf4582348218bbff07eb4121fdef2439bc02de6ca5eef7efdc8","tx_hash":"22135352edeeaaf4582348218bbff07eb4121fdef2439bc02de6ca5eef7efdc8","message":"Payment Sent","success":true,"warning":"Using a static fee amount may cause large transactions to confirm slowly"}', true);

            if(isset($result['error'])){
                $this->balance = $oldBalance;
                return $result;
            }

            $this->save(false);
            return $result;

//
//            $ch = curl_init();
//
//            // установка URL и других необходимых параметров
//            curl_setopt($ch, CURLOPT_URL, "http://localhost:3000/merchant/{$adminGuid}/payment?password={$adminPassword}&from=1&to=1FPzkLxY4Gfk3Ft2aRLq4k2yqiUskh5srZ&amount=3400");
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//
//            // загрузка страницы и выдача её браузеру
//            $result = curl_exec($ch);
//
//            var_dump($result);
//
//            // завершение сеанса и освобождение ресурсов
//            curl_close($ch);
        }

        $this->save(false);
        return $result;
    }

    /**
     * @return array
     */
    public function withdraw($amount, $to)
    {
        /** @var $setting Settings */
        $isDemo = Settings::getBooleanValueByKey('demo_transaction_mode');
        $adminGuid = Settings::findByKey('exchange_token')->value;
        $adminPassword = Settings::findByKey('exchange_password')->value;
        $commissionBtc = Settings::findByKey('commission_transaction_value')->value;
        $oldBalance = $this->balance;

        if($isDemo == false)
        {
            /** @var AdminBalance $adminBalance */
            $adminBalance = Yii::$app->adminBalance;

            $this->balance -= $amount;

            $amount = $amount - floatval($commissionBtc);

            $amountSatoshi = $amount * 100000000;

            $account = $this->blockchainAccount;

            $result = $account->payment($to, $amountSatoshi);


            // $result = json_decode('{"to":["19HnoZpLZV3Krn2kHRmLZmUW6J2NWf6i6h"],"amounts":[1096],"from":["xpub6DEE5k4gyw3TmJF17W6AoAPBFprpo6ayRLeRMSvtFN1uh9aynfFroXTi8Ctx1zYxS5wFftmvcANHZHXD6jpGMTx6tyCaQ38oN8ivae4QiyR"],"fee":1000,"txid":"0a2c9532ed2e409f5801ca9ce3214c635246d3eec8cfda3b53d97fd50ec9885f","tx_hash":"0a2c9532ed2e409f5801ca9ce3214c635246d3eec8cfda3b53d97fd50ec9885f","message":"Payment Sent","success":true,"warning":"Using a static fee amount may cause large transactions to confirm slowly"}', true);

            if(isset($result['error'])){
                $this->balance = $oldBalance;
                \Yii::warning($result['error']);
                return $result;
            }

            $this->save();
            return $result;
        } else {
            $this->balance -= $amount;            
        }


        $this->save();
        return [];
    }

    public function getBlockchainAccount()
    {
        return new BlockchainAccount([
            'guid' => $this->guid,
            'password' => $this->password
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobileUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'mobile_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['wallet_from_id' => 'id']);
    }
}
