<?php

namespace app\models\forms;

use app\models\MobileUserFile;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadFileForm
 * @package app\models\forms
 */
class UploadFileForm extends Model
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $type;

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'type'], 'required'],
            [['userId', 'type'], 'integer'],
            ['file', 'file'],
        ];
    }

    /**
     * @return bool
     */
    public function upload()
    {
        $this->file = UploadedFile::getInstanceByName('file');

        if(is_dir('uploads') == false){
            mkdir('uploads');
        }

        $name = Yii::$app->security->generateRandomString();
        $path = "uploads/{$name}.{$this->file->extension}";

        $this->file->saveAs($path);

        $file = new MobileUserFile([
            'mobile_user_id' => $this->userId,
            'name' => $this->file->baseName,
            'type' => $this->type,
            'path' => $path,
        ]);

        return $file->save(false);
    }
}