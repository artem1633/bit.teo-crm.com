<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MUserWallet */
?>
<div class="muser-wallet-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mobile_user_id',
            // 'token',
            'password',
            'balance',
            'created_at',
            'type',
        ],
    ]) ?>

</div>
