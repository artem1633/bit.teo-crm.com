<?php

namespace app\components;

class FcmPusher {

	const TYPE_FORM = 'form';
	const TYPE_TICKET = 'ticket';

	public static function push($recipient, $message, $data = [], $default = true)
	{
        $url = 'https://fcm.googleapis.com/fcm/send';

        $request_body = [
            'to' => $recipient,
            'notification' => [
                'title' => 'Сообщение сервиса FaCoin',
                'body' => $message,
//                'icon' => 'push-button.png',
//                'click_action' => 'https://sergdudko.tk',
            ],
            'data' => $data,
        ];
        $fields = json_encode($request_body);

        if($default){
            $key = 'AAAAksN_IIQ:APA91bFlvx2rZlJECj7I2PbTBmMFgtXfQycmuYsgvTdLaHzgKpwo4GEP1V0iW8UV_P0ORcR2LYwHz9GRRYZiWjxhLlthonQlnXSJHt1vG3SIAS4MrjvekUe6Os4Jfyom5eYi5fLBZMnf';
        } else {
            $key = 'AAAAexDRkxY:APA91bF4XmVcegtT0WurQkJz6TsCngjbZlKV6wCGm8QNSf5kRKOGinHFBkLdVtcEVGdiaukMbIkzX7NtmuMT2Y1UjUcBnKzqkkhMlTkiDSfhRWc8phBhzV-NfWSkVUZgIcmsfU6hIEr6';
        }

        $request_headers = [
            'Content-Type: application/json',
            "Authorization: key={$key}",
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);

		\Yii::warning($response, 'Push send response');

        return $response;
	}
}