<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mobile_user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $name ФИО
 * @property string $password Пароль
 * @property string $email Email
 * @property string $token Токен
 * @property string $fcm_token FCM токен
 * @property string $created_at Дата и время регистрации
 * @property string $last_login_datetime Дата и время последней авторизации
 *
 * @property MUserWallet[] $mUserWallets
 * @property Ticket[] $tickets
 * @property Transaction[] $transactions
 */
class MobileUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'last_login_datetime'], 'safe'],
            [['login', 'password', 'email', 'token', 'name', 'country'], 'string', 'max' => 255],
            [['fcm_token'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'email' => 'Email',
            'token' => 'Токен',
            'name' => 'ФИО',
            'country' => 'Страна',
            'fcm_token' => 'FCM токен',
            'created_at' => 'Дата и время регистрации',
            'last_login_datetime' => 'Дата и время последней авторизации',
        ];
    }

    public static function getCountries()
    {
        return require(Yii::getAlias('@app/data/countries.php'));
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->token = md5($this->login).md5($this->password);

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMUserWallets()
    {
        return $this->hasMany(MUserWallet::className(), ['mobile_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::className(), ['mobile_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['mobile_user_id' => 'id']);
    }
}
